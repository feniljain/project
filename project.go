package projects

//NOTE: Please ignore formatting issues, I kind of messed up my go config for vim and hence gofmt aint working, I will try to get it working asap.

import (
	"context"
	"fmt"

	cherrors "go.saastack.io/chaku/errors"
	"go.saastack.io/idutil"
	locationpb "go.saastack.io/location/pb"
	"go.saastack.io/project/pb"
	"google.golang.org/genproto/protobuf/field_mask"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	// Generic Error to be returned to client to hide possible sensitive information
	errInternal = status.Error(codes.Internal, "oops! Something went wrong")
    locationPrefix = (&locationpb.Location{}).GetPrefix()
    errInvalidParent = status.Error(codes.InvalidArgument, "chaku: Invalid Parent")
    errInvalidField = status.Error(codes.InvalidArgument, "chaku: Invalid Field(s)")
    errAlreadyExist = status.Error(codes.AlreadyExists, "chaku: Already Exists")
)

type projectsServer struct {
	projectStore pb.ProjectStore
	projectBLoC  pb.ProjectsServiceProjectServerBLoC
	*pb.ProjectsServiceProjectServerCrud

    parentServer pb.ParentServiceClient
}

func NewProjectsServer(

	projectSt pb.ProjectStore,

    parentServer pb.ParentServiceClient,


) pb.ProjectsServer {
	r := &projectsServer{

		projectStore: projectSt,
	}

	projectSC := pb.NewProjectsServiceProjectServerCrud(projectSt, r)
	r.ProjectsServiceProjectServerCrud = projectSC

	return r
}

// These functions represent the BLoC(Business Logical Component) of the CRUDGen Server.
// These functions will contain business logic and would be called inside the generated CRUD functions

func (s *projectsServer) CreateProjectBLoC(ctx context.Context, in *pb.CreateProjectRequest) error {

    //TODO: Check and test location parent [X]
    //TODO: Check and test title already exist [X]
    if idutil.GetPrefix(in.GetParent()) != locationPrefix {
        return errInvalidParent
    }

    _, err := s.projectStore.GetProject(ctx, []string{}, pb.ProjectTitleEq{Title: in.GetProject().GetTitle()})
    if err!=nil {
        if err == cherrors.ErrNotFound {
            return nil
        }
        return err
    }

	return errAlreadyExist
}

func (s *projectsServer) GetProjectBLoC(ctx context.Context, in *pb.GetProjectRequest) error {
    //TODO: Test it [X]
	return nil
}

func (s *projectsServer) UpdateProjectBLoC(ctx context.Context, in *pb.UpdateProjectRequest) error {

    //Validate fields
    if err := in.Validate(); err!=nil {
        return err
    }

    for _, m := range in.GetUpdateMask().GetPaths() {
		if !pb.ValidProjectFieldMask(m) {
			return cherrors.ErrInvalidField
		}

		if m == "id" {
			return cherrors.ErrInvalidField
		}
	}

    //TODO: Check and test title already exist [X]
    _, err := s.projectStore.GetProject(ctx, []string{}, pb.ProjectTitleEq{Title: in.GetProject().GetTitle()})
    if err!=nil {
        if err == cherrors.ErrNotFound {
			return nil
		}
		return err
    }

	return cherrors.ErrObjIdExist
}

func (s *projectsServer) DeleteProjectBLoC(ctx context.Context, in *pb.DeleteProjectRequest) error {
    //TODO: Test it [X]
	return nil
}

func (s *projectsServer) BatchGetProjectBLoC(ctx context.Context, in *pb.BatchGetProjectRequest) error {
	return nil
}

func (s *projectsServer) ListProjectBLoC(ctx context.Context, in *pb.ListProjectRequest) (pb.ProjectCondition, error) {

    //Validating fields not required as already done in generated code

    //TODO: Check and test the returning condition properly [X]

	// Validate view masks
	for _, m := range in.GetViewMask().GetPaths() {
		if !pb.ValidProjectFieldMask(m) {
			return nil, cherrors.ErrInvalidField
		}
	}

	if in.GetParent() != "" {
		//return pb.ProjectParentEq{Parent: in.GetParent()}, nil
	    return pb.ProjectParentEq{Parent: idutil.GetId(in.GetParent())}, nil
	}

	return nil, errInvalidField
}

// These functions are not implemented by CRUDGen, needed to be implemented

//Helper struct for working with validation using external services
type parentServiceServer struct {
	lCli locationpb.LocationsClient
}

//NewParentServiceServer returns a ParentServiceServer implementation with core business logic
func NewParentServiceServer(lCli locationpb.LocationsClient) pb.ParentServiceServer {
	return &parentServiceServer{lCli}
}

//ValidateParent checks for valid project ID
func (s *parentServiceServer) ValidateParent(ctx context.Context, in *pb.ValidateParentRequest) (*pb.ValidateParentResponse, error) {
    fmt.Println("Here1")
     _, err := s.lCli.GetLocation(ctx, &locationpb.GetLocationRequest{Id: in.GetId(), ViewMask: &field_mask.FieldMask{Paths: []string{"id"}}})
    if err!=nil {
        fmt.Println("Here2")
        return nil, errInvalidParent
    }

    fmt.Println("Here3")
    return &pb.ValidateParentResponse{Valid: true}, nil
}

