// Code generated by protoc-gen-go. DO NOT EDIT.
// source: project.proto

package pb

import (
	context "context"
	fmt "fmt"
	math "math"

	_ "github.com/Shivam010/protoc-gen-validate/validate"
	proto "github.com/golang/protobuf/proto"
	empty "github.com/golang/protobuf/ptypes/empty"
	_ "go.saastack.io/chaku/validate"
	_ "go.saastack.io/events/eventspush"
	_ "go.saastack.io/jaal/schema"
	_ "go.saastack.io/pehredaar/pehredaar"
	_ "google.golang.org/genproto/googleapis/api/annotations"
	field_mask "google.golang.org/genproto/protobuf/field_mask"
	grpc "google.golang.org/grpc"
	codes "google.golang.org/grpc/codes"
	status "google.golang.org/grpc/status"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

type Project struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Title                string   `protobuf:"bytes,2,opt,name=title,proto3" json:"title,omitempty"`
	Color                string   `protobuf:"bytes,3,opt,name=color,proto3" json:"color,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *Project) Reset()         { *m = Project{} }
func (m *Project) String() string { return proto.CompactTextString(m) }
func (*Project) ProtoMessage()    {}
func (*Project) Descriptor() ([]byte, []int) {
	return fileDescriptor_8340e6318dfdfac2, []int{0}
}

func (m *Project) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_Project.Unmarshal(m, b)
}
func (m *Project) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_Project.Marshal(b, m, deterministic)
}
func (m *Project) XXX_Merge(src proto.Message) {
	xxx_messageInfo_Project.Merge(m, src)
}
func (m *Project) XXX_Size() int {
	return xxx_messageInfo_Project.Size(m)
}
func (m *Project) XXX_DiscardUnknown() {
	xxx_messageInfo_Project.DiscardUnknown(m)
}

var xxx_messageInfo_Project proto.InternalMessageInfo

func (m *Project) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *Project) GetTitle() string {
	if m != nil {
		return m.Title
	}
	return ""
}

func (m *Project) GetColor() string {
	if m != nil {
		return m.Color
	}
	return ""
}

type CreateProjectRequest struct {
	Parent               string   `protobuf:"bytes,1,opt,name=parent,proto3" json:"parent,omitempty"`
	Project              *Project `protobuf:"bytes,2,opt,name=project,proto3" json:"project,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *CreateProjectRequest) Reset()         { *m = CreateProjectRequest{} }
func (m *CreateProjectRequest) String() string { return proto.CompactTextString(m) }
func (*CreateProjectRequest) ProtoMessage()    {}
func (*CreateProjectRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_8340e6318dfdfac2, []int{1}
}

func (m *CreateProjectRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_CreateProjectRequest.Unmarshal(m, b)
}
func (m *CreateProjectRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_CreateProjectRequest.Marshal(b, m, deterministic)
}
func (m *CreateProjectRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_CreateProjectRequest.Merge(m, src)
}
func (m *CreateProjectRequest) XXX_Size() int {
	return xxx_messageInfo_CreateProjectRequest.Size(m)
}
func (m *CreateProjectRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_CreateProjectRequest.DiscardUnknown(m)
}

var xxx_messageInfo_CreateProjectRequest proto.InternalMessageInfo

func (m *CreateProjectRequest) GetParent() string {
	if m != nil {
		return m.Parent
	}
	return ""
}

func (m *CreateProjectRequest) GetProject() *Project {
	if m != nil {
		return m.Project
	}
	return nil
}

type GetProjectRequest struct {
	Id                   string                `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	ViewMask             *field_mask.FieldMask `protobuf:"bytes,2,opt,name=view_mask,json=viewMask,proto3" json:"view_mask,omitempty"`
	XXX_NoUnkeyedLiteral struct{}              `json:"-"`
	XXX_unrecognized     []byte                `json:"-"`
	XXX_sizecache        int32                 `json:"-"`
}

func (m *GetProjectRequest) Reset()         { *m = GetProjectRequest{} }
func (m *GetProjectRequest) String() string { return proto.CompactTextString(m) }
func (*GetProjectRequest) ProtoMessage()    {}
func (*GetProjectRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_8340e6318dfdfac2, []int{2}
}

func (m *GetProjectRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_GetProjectRequest.Unmarshal(m, b)
}
func (m *GetProjectRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_GetProjectRequest.Marshal(b, m, deterministic)
}
func (m *GetProjectRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_GetProjectRequest.Merge(m, src)
}
func (m *GetProjectRequest) XXX_Size() int {
	return xxx_messageInfo_GetProjectRequest.Size(m)
}
func (m *GetProjectRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_GetProjectRequest.DiscardUnknown(m)
}

var xxx_messageInfo_GetProjectRequest proto.InternalMessageInfo

func (m *GetProjectRequest) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

func (m *GetProjectRequest) GetViewMask() *field_mask.FieldMask {
	if m != nil {
		return m.ViewMask
	}
	return nil
}

type DeleteProjectRequest struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *DeleteProjectRequest) Reset()         { *m = DeleteProjectRequest{} }
func (m *DeleteProjectRequest) String() string { return proto.CompactTextString(m) }
func (*DeleteProjectRequest) ProtoMessage()    {}
func (*DeleteProjectRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_8340e6318dfdfac2, []int{3}
}

func (m *DeleteProjectRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_DeleteProjectRequest.Unmarshal(m, b)
}
func (m *DeleteProjectRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_DeleteProjectRequest.Marshal(b, m, deterministic)
}
func (m *DeleteProjectRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_DeleteProjectRequest.Merge(m, src)
}
func (m *DeleteProjectRequest) XXX_Size() int {
	return xxx_messageInfo_DeleteProjectRequest.Size(m)
}
func (m *DeleteProjectRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_DeleteProjectRequest.DiscardUnknown(m)
}

var xxx_messageInfo_DeleteProjectRequest proto.InternalMessageInfo

func (m *DeleteProjectRequest) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

type UpdateProjectRequest struct {
	Project              *Project              `protobuf:"bytes,1,opt,name=project,proto3" json:"project,omitempty"`
	UpdateMask           *field_mask.FieldMask `protobuf:"bytes,2,opt,name=update_mask,json=updateMask,proto3" json:"update_mask,omitempty"`
	XXX_NoUnkeyedLiteral struct{}              `json:"-"`
	XXX_unrecognized     []byte                `json:"-"`
	XXX_sizecache        int32                 `json:"-"`
}

func (m *UpdateProjectRequest) Reset()         { *m = UpdateProjectRequest{} }
func (m *UpdateProjectRequest) String() string { return proto.CompactTextString(m) }
func (*UpdateProjectRequest) ProtoMessage()    {}
func (*UpdateProjectRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_8340e6318dfdfac2, []int{4}
}

func (m *UpdateProjectRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_UpdateProjectRequest.Unmarshal(m, b)
}
func (m *UpdateProjectRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_UpdateProjectRequest.Marshal(b, m, deterministic)
}
func (m *UpdateProjectRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_UpdateProjectRequest.Merge(m, src)
}
func (m *UpdateProjectRequest) XXX_Size() int {
	return xxx_messageInfo_UpdateProjectRequest.Size(m)
}
func (m *UpdateProjectRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_UpdateProjectRequest.DiscardUnknown(m)
}

var xxx_messageInfo_UpdateProjectRequest proto.InternalMessageInfo

func (m *UpdateProjectRequest) GetProject() *Project {
	if m != nil {
		return m.Project
	}
	return nil
}

func (m *UpdateProjectRequest) GetUpdateMask() *field_mask.FieldMask {
	if m != nil {
		return m.UpdateMask
	}
	return nil
}

type ListProjectRequest struct {
	// Parent is a fully qualified string that contains information about the
	// owner in hierarchical manner group/location/business (required)
	Parent               string                `protobuf:"bytes,1,opt,name=parent,proto3" json:"parent,omitempty"`
	ViewMask             *field_mask.FieldMask `protobuf:"bytes,2,opt,name=view_mask,json=viewMask,proto3" json:"view_mask,omitempty"`
	XXX_NoUnkeyedLiteral struct{}              `json:"-"`
	XXX_unrecognized     []byte                `json:"-"`
	XXX_sizecache        int32                 `json:"-"`
}

func (m *ListProjectRequest) Reset()         { *m = ListProjectRequest{} }
func (m *ListProjectRequest) String() string { return proto.CompactTextString(m) }
func (*ListProjectRequest) ProtoMessage()    {}
func (*ListProjectRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_8340e6318dfdfac2, []int{5}
}

func (m *ListProjectRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListProjectRequest.Unmarshal(m, b)
}
func (m *ListProjectRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListProjectRequest.Marshal(b, m, deterministic)
}
func (m *ListProjectRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListProjectRequest.Merge(m, src)
}
func (m *ListProjectRequest) XXX_Size() int {
	return xxx_messageInfo_ListProjectRequest.Size(m)
}
func (m *ListProjectRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ListProjectRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ListProjectRequest proto.InternalMessageInfo

func (m *ListProjectRequest) GetParent() string {
	if m != nil {
		return m.Parent
	}
	return ""
}

func (m *ListProjectRequest) GetViewMask() *field_mask.FieldMask {
	if m != nil {
		return m.ViewMask
	}
	return nil
}

type ListProjectResponse struct {
	Project              []*Project `protobuf:"bytes,1,rep,name=project,proto3" json:"project,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *ListProjectResponse) Reset()         { *m = ListProjectResponse{} }
func (m *ListProjectResponse) String() string { return proto.CompactTextString(m) }
func (*ListProjectResponse) ProtoMessage()    {}
func (*ListProjectResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_8340e6318dfdfac2, []int{6}
}

func (m *ListProjectResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ListProjectResponse.Unmarshal(m, b)
}
func (m *ListProjectResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ListProjectResponse.Marshal(b, m, deterministic)
}
func (m *ListProjectResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ListProjectResponse.Merge(m, src)
}
func (m *ListProjectResponse) XXX_Size() int {
	return xxx_messageInfo_ListProjectResponse.Size(m)
}
func (m *ListProjectResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ListProjectResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ListProjectResponse proto.InternalMessageInfo

func (m *ListProjectResponse) GetProject() []*Project {
	if m != nil {
		return m.Project
	}
	return nil
}

type BatchGetProjectRequest struct {
	Ids                  []string              `protobuf:"bytes,1,rep,name=ids,proto3" json:"ids,omitempty"`
	ViewMask             *field_mask.FieldMask `protobuf:"bytes,2,opt,name=view_mask,json=viewMask,proto3" json:"view_mask,omitempty"`
	XXX_NoUnkeyedLiteral struct{}              `json:"-"`
	XXX_unrecognized     []byte                `json:"-"`
	XXX_sizecache        int32                 `json:"-"`
}

func (m *BatchGetProjectRequest) Reset()         { *m = BatchGetProjectRequest{} }
func (m *BatchGetProjectRequest) String() string { return proto.CompactTextString(m) }
func (*BatchGetProjectRequest) ProtoMessage()    {}
func (*BatchGetProjectRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_8340e6318dfdfac2, []int{7}
}

func (m *BatchGetProjectRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BatchGetProjectRequest.Unmarshal(m, b)
}
func (m *BatchGetProjectRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BatchGetProjectRequest.Marshal(b, m, deterministic)
}
func (m *BatchGetProjectRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BatchGetProjectRequest.Merge(m, src)
}
func (m *BatchGetProjectRequest) XXX_Size() int {
	return xxx_messageInfo_BatchGetProjectRequest.Size(m)
}
func (m *BatchGetProjectRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_BatchGetProjectRequest.DiscardUnknown(m)
}

var xxx_messageInfo_BatchGetProjectRequest proto.InternalMessageInfo

func (m *BatchGetProjectRequest) GetIds() []string {
	if m != nil {
		return m.Ids
	}
	return nil
}

func (m *BatchGetProjectRequest) GetViewMask() *field_mask.FieldMask {
	if m != nil {
		return m.ViewMask
	}
	return nil
}

type BatchGetProjectResponse struct {
	Project              []*Project `protobuf:"bytes,1,rep,name=project,proto3" json:"project,omitempty"`
	XXX_NoUnkeyedLiteral struct{}   `json:"-"`
	XXX_unrecognized     []byte     `json:"-"`
	XXX_sizecache        int32      `json:"-"`
}

func (m *BatchGetProjectResponse) Reset()         { *m = BatchGetProjectResponse{} }
func (m *BatchGetProjectResponse) String() string { return proto.CompactTextString(m) }
func (*BatchGetProjectResponse) ProtoMessage()    {}
func (*BatchGetProjectResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_8340e6318dfdfac2, []int{8}
}

func (m *BatchGetProjectResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_BatchGetProjectResponse.Unmarshal(m, b)
}
func (m *BatchGetProjectResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_BatchGetProjectResponse.Marshal(b, m, deterministic)
}
func (m *BatchGetProjectResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_BatchGetProjectResponse.Merge(m, src)
}
func (m *BatchGetProjectResponse) XXX_Size() int {
	return xxx_messageInfo_BatchGetProjectResponse.Size(m)
}
func (m *BatchGetProjectResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_BatchGetProjectResponse.DiscardUnknown(m)
}

var xxx_messageInfo_BatchGetProjectResponse proto.InternalMessageInfo

func (m *BatchGetProjectResponse) GetProject() []*Project {
	if m != nil {
		return m.Project
	}
	return nil
}

type ValidateParentRequest struct {
	Id                   string   `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ValidateParentRequest) Reset()         { *m = ValidateParentRequest{} }
func (m *ValidateParentRequest) String() string { return proto.CompactTextString(m) }
func (*ValidateParentRequest) ProtoMessage()    {}
func (*ValidateParentRequest) Descriptor() ([]byte, []int) {
	return fileDescriptor_8340e6318dfdfac2, []int{9}
}

func (m *ValidateParentRequest) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ValidateParentRequest.Unmarshal(m, b)
}
func (m *ValidateParentRequest) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ValidateParentRequest.Marshal(b, m, deterministic)
}
func (m *ValidateParentRequest) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ValidateParentRequest.Merge(m, src)
}
func (m *ValidateParentRequest) XXX_Size() int {
	return xxx_messageInfo_ValidateParentRequest.Size(m)
}
func (m *ValidateParentRequest) XXX_DiscardUnknown() {
	xxx_messageInfo_ValidateParentRequest.DiscardUnknown(m)
}

var xxx_messageInfo_ValidateParentRequest proto.InternalMessageInfo

func (m *ValidateParentRequest) GetId() string {
	if m != nil {
		return m.Id
	}
	return ""
}

type ValidateParentResponse struct {
	Valid                bool     `protobuf:"varint,1,opt,name=valid,proto3" json:"valid,omitempty"`
	XXX_NoUnkeyedLiteral struct{} `json:"-"`
	XXX_unrecognized     []byte   `json:"-"`
	XXX_sizecache        int32    `json:"-"`
}

func (m *ValidateParentResponse) Reset()         { *m = ValidateParentResponse{} }
func (m *ValidateParentResponse) String() string { return proto.CompactTextString(m) }
func (*ValidateParentResponse) ProtoMessage()    {}
func (*ValidateParentResponse) Descriptor() ([]byte, []int) {
	return fileDescriptor_8340e6318dfdfac2, []int{10}
}

func (m *ValidateParentResponse) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ValidateParentResponse.Unmarshal(m, b)
}
func (m *ValidateParentResponse) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ValidateParentResponse.Marshal(b, m, deterministic)
}
func (m *ValidateParentResponse) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ValidateParentResponse.Merge(m, src)
}
func (m *ValidateParentResponse) XXX_Size() int {
	return xxx_messageInfo_ValidateParentResponse.Size(m)
}
func (m *ValidateParentResponse) XXX_DiscardUnknown() {
	xxx_messageInfo_ValidateParentResponse.DiscardUnknown(m)
}

var xxx_messageInfo_ValidateParentResponse proto.InternalMessageInfo

func (m *ValidateParentResponse) GetValid() bool {
	if m != nil {
		return m.Valid
	}
	return false
}

func init() {
	proto.RegisterType((*Project)(nil), "saastack.project.v1.Project")
	proto.RegisterType((*CreateProjectRequest)(nil), "saastack.project.v1.CreateProjectRequest")
	proto.RegisterType((*GetProjectRequest)(nil), "saastack.project.v1.GetProjectRequest")
	proto.RegisterType((*DeleteProjectRequest)(nil), "saastack.project.v1.DeleteProjectRequest")
	proto.RegisterType((*UpdateProjectRequest)(nil), "saastack.project.v1.UpdateProjectRequest")
	proto.RegisterType((*ListProjectRequest)(nil), "saastack.project.v1.ListProjectRequest")
	proto.RegisterType((*ListProjectResponse)(nil), "saastack.project.v1.ListProjectResponse")
	proto.RegisterType((*BatchGetProjectRequest)(nil), "saastack.project.v1.BatchGetProjectRequest")
	proto.RegisterType((*BatchGetProjectResponse)(nil), "saastack.project.v1.BatchGetProjectResponse")
	proto.RegisterType((*ValidateParentRequest)(nil), "saastack.project.v1.ValidateParentRequest")
	proto.RegisterType((*ValidateParentResponse)(nil), "saastack.project.v1.ValidateParentResponse")
}

func init() { proto.RegisterFile("project.proto", fileDescriptor_8340e6318dfdfac2) }

var fileDescriptor_8340e6318dfdfac2 = []byte{
	// 866 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xa4, 0x55, 0x4f, 0x8f, 0xdb, 0x44,
	0x14, 0xd7, 0x38, 0xce, 0x6e, 0xf2, 0xd2, 0x6c, 0x77, 0x67, 0xb3, 0xff, 0x0c, 0x85, 0xd6, 0x87,
	0x52, 0xd2, 0x60, 0x77, 0x03, 0x02, 0x29, 0xf4, 0x42, 0xf8, 0x77, 0x61, 0xa5, 0x62, 0x44, 0x0f,
	0x5c, 0xaa, 0x89, 0x3d, 0x4d, 0x4c, 0xfe, 0xd8, 0xd8, 0x93, 0x54, 0x50, 0xe5, 0xb2, 0x47, 0x24,
	0x10, 0x12, 0x08, 0xa4, 0x7c, 0x01, 0xbe, 0xc3, 0x1e, 0xa2, 0x7e, 0x07, 0x3e, 0xc2, 0x72, 0x81,
	0x43, 0x0e, 0x9c, 0x38, 0x22, 0xcf, 0x8c, 0x13, 0x3b, 0xf1, 0x92, 0x68, 0x7b, 0x89, 0x67, 0xe6,
	0xcd, 0x7b, 0xbf, 0xdf, 0x7b, 0xbf, 0x79, 0x2f, 0x50, 0xf6, 0x03, 0xef, 0x6b, 0x6a, 0x33, 0xc3,
	0x0f, 0x3c, 0xe6, 0xe1, 0xfd, 0x90, 0x90, 0x90, 0x11, 0xbb, 0x6b, 0xc4, 0xe7, 0xa3, 0x53, 0xed,
	0x16, 0x19, 0x0c, 0x3c, 0x46, 0x98, 0xeb, 0x0d, 0x42, 0x33, 0xb1, 0x16, 0x3e, 0xda, 0x2b, 0x6d,
	0xcf, 0x6b, 0xf7, 0xa8, 0xc9, 0x77, 0xad, 0xe1, 0x53, 0x93, 0xf6, 0x7d, 0xf6, 0xad, 0x34, 0xde,
	0x5e, 0x36, 0x3e, 0x75, 0x69, 0xcf, 0x79, 0xd2, 0x27, 0x61, 0x57, 0xde, 0x38, 0x1a, 0x91, 0x9e,
	0xeb, 0x10, 0x46, 0xcd, 0x78, 0x21, 0x0d, 0x95, 0xb9, 0xc1, 0xee, 0x90, 0xee, 0x50, 0x9e, 0x9e,
	0xf8, 0xb4, 0x13, 0x50, 0x87, 0x90, 0xc0, 0x9c, 0xaf, 0xa4, 0xe9, 0x80, 0x8e, 0xe8, 0x80, 0x85,
	0xfe, 0x30, 0xec, 0x98, 0xd1, 0x8f, 0x3c, 0xde, 0x0f, 0xed, 0x0e, 0xed, 0x13, 0x53, 0x7c, 0xc4,
	0xa1, 0xfe, 0x04, 0xb6, 0x1f, 0x89, 0x0c, 0xf1, 0x0e, 0x28, 0xae, 0x73, 0x8c, 0x6e, 0xa3, 0x7b,
	0x45, 0x4b, 0x71, 0x1d, 0x7c, 0x07, 0xf2, 0xcc, 0x65, 0x3d, 0x7a, 0xac, 0x44, 0x47, 0xcd, 0xd2,
	0xe4, 0xf2, 0x45, 0x6e, 0x2b, 0x50, 0x77, 0x73, 0xc7, 0x8e, 0x25, 0x2c, 0xb8, 0x02, 0x79, 0xdb,
	0xeb, 0x79, 0xc1, 0x71, 0x8e, 0x7b, 0x89, 0x4d, 0xa3, 0xf4, 0xef, 0x5f, 0x0f, 0xd0, 0xf9, 0xdf,
	0x0f, 0x72, 0x7e, 0xe0, 0xe9, 0x63, 0xa8, 0x7c, 0x18, 0x50, 0xc2, 0xa8, 0x84, 0xb1, 0xe8, 0x37,
	0x43, 0x1a, 0x32, 0x7c, 0x07, 0xb6, 0x7c, 0x12, 0xd0, 0x01, 0x13, 0x88, 0xcd, 0x62, 0x14, 0x5e,
	0x0d, 0x94, 0xdd, 0x9c, 0x25, 0x0d, 0xb8, 0x09, 0xdb, 0xb2, 0xfa, 0x9c, 0x42, 0xa9, 0xfe, 0xaa,
	0x91, 0x21, 0x8b, 0x21, 0x03, 0x37, 0x21, 0x8a, 0x90, 0xff, 0x1e, 0x29, 0xbb, 0xc8, 0x8a, 0x1d,
	0xf5, 0x36, 0xec, 0x7d, 0x4a, 0xd9, 0x12, 0xf6, 0xc9, 0x22, 0xd3, 0x24, 0x6e, 0x94, 0xf4, 0x7b,
	0x50, 0x1c, 0xb9, 0xf4, 0x19, 0x17, 0x46, 0xa2, 0x6a, 0x86, 0xd0, 0xce, 0x88, 0xb5, 0x33, 0x3e,
	0x89, 0xb4, 0x3b, 0x23, 0x61, 0xd7, 0x2a, 0x44, 0x97, 0xa3, 0x95, 0x7e, 0x0a, 0x95, 0x8f, 0x68,
	0x8f, 0xae, 0xe4, 0x79, 0x35, 0x96, 0xfe, 0x1b, 0x82, 0xca, 0x97, 0xbe, 0xb3, 0x5a, 0x9b, 0x44,
	0xe2, 0xe8, 0x9a, 0x89, 0xe3, 0xf7, 0xa1, 0x34, 0xe4, 0xb1, 0x37, 0x4d, 0x05, 0xc4, 0x75, 0x9e,
	0x8c, 0x0f, 0xf8, 0x33, 0x37, 0x64, 0x9b, 0x4a, 0x86, 0xe6, 0x92, 0x5d, 0xbb, 0x7c, 0x67, 0xb0,
	0x9f, 0x42, 0x0c, 0x7d, 0x6f, 0x10, 0x52, 0xfc, 0x6e, 0xb2, 0x12, 0xb9, 0x75, 0x95, 0x58, 0xc8,
	0x6e, 0xc3, 0x61, 0x93, 0x30, 0xbb, 0xb3, 0xaa, 0xfd, 0x2e, 0xe4, 0x5c, 0x27, 0xe4, 0xd1, 0x8a,
	0x56, 0xb4, 0xbc, 0x3e, 0xe7, 0xcf, 0xe1, 0x68, 0x05, 0xe4, 0x25, 0x79, 0xbf, 0x05, 0x07, 0x8f,
	0x65, 0xb7, 0x3f, 0xe2, 0x15, 0x8d, 0x69, 0x2f, 0x35, 0x67, 0x43, 0x7d, 0x31, 0xad, 0x21, 0xfd,
	0x1d, 0x38, 0x5c, 0xbe, 0x2e, 0x09, 0x54, 0x20, 0xcf, 0xc7, 0x06, 0x77, 0x29, 0x58, 0x62, 0x23,
	0xbc, 0xea, 0x3f, 0x6c, 0x43, 0x41, 0x22, 0x87, 0xf8, 0x57, 0x04, 0xe5, 0x54, 0x83, 0xe2, 0x37,
	0x33, 0xa9, 0x66, 0x35, 0xb1, 0xf6, 0xbf, 0x59, 0xe9, 0x0f, 0x27, 0x33, 0xb5, 0x10, 0xbf, 0x99,
	0x7f, 0x66, 0xaa, 0x52, 0x40, 0x17, 0xd3, 0xda, 0x4d, 0x5c, 0xb6, 0x93, 0xb1, 0xce, 0xff, 0xf8,
	0xf3, 0x67, 0x65, 0x4f, 0xbf, 0x61, 0x8e, 0x4e, 0x4d, 0x19, 0x24, 0x6c, 0xa0, 0x2a, 0x3e, 0x47,
	0x00, 0x8b, 0xca, 0xe2, 0xbb, 0x99, 0x50, 0x2b, 0xfa, 0xae, 0xa1, 0x64, 0x4c, 0x66, 0xaa, 0x1a,
	0x95, 0xf2, 0x62, 0x5a, 0x2b, 0xce, 0x35, 0xe2, 0x14, 0xf6, 0xf1, 0x5e, 0x92, 0x82, 0xf9, 0xdc,
	0x75, 0xc6, 0xf8, 0x47, 0x04, 0xe5, 0x54, 0x5b, 0x5f, 0x51, 0x9d, 0xac, 0xd6, 0xd7, 0x0e, 0x57,
	0x5e, 0xd1, 0xc7, 0xd1, 0x3f, 0x82, 0xde, 0x88, 0x49, 0x24, 0x6b, 0xe2, 0x24, 0x23, 0x08, 0x42,
	0xd5, 0x0c, 0x42, 0xbf, 0x23, 0x28, 0xa7, 0x66, 0xc6, 0x15, 0x84, 0xb2, 0xe6, 0xca, 0x9a, 0xda,
	0x9c, 0x4d, 0x66, 0xea, 0x0d, 0x80, 0xd8, 0x92, 0xa6, 0x37, 0x4c, 0xc6, 0xe3, 0xf4, 0x5e, 0xd3,
	0x4e, 0xd2, 0xf4, 0x16, 0x8e, 0xe3, 0x48, 0xbf, 0x9f, 0x10, 0x94, 0x12, 0x2d, 0x8d, 0xdf, 0xc8,
	0x04, 0x5f, 0x1d, 0x33, 0xda, 0xbd, 0xf5, 0x17, 0xc5, 0x23, 0xe7, 0x6a, 0xce, 0x1f, 0xd8, 0xc5,
	0xb4, 0x06, 0x50, 0x88, 0xe9, 0x70, 0x8a, 0x3b, 0x38, 0xf5, 0xaa, 0xf0, 0x2f, 0x08, 0x6e, 0x2e,
	0x75, 0x2c, 0xbe, 0x9f, 0x89, 0x96, 0x3d, 0x3c, 0xb4, 0xda, 0x66, 0x97, 0x25, 0xbd, 0xbb, 0x93,
	0x99, 0x9a, 0xe7, 0xe3, 0x86, 0xb3, 0x39, 0xc2, 0x07, 0xa9, 0x37, 0xde, 0x92, 0x6e, 0xf5, 0xef,
	0xa0, 0x2c, 0xba, 0xf7, 0x0b, 0x1a, 0x8c, 0x5c, 0x9b, 0x62, 0x17, 0x76, 0xd2, 0x6d, 0x8d, 0xab,
	0x99, 0xc0, 0x99, 0xa3, 0x42, 0xbb, 0xbf, 0xd1, 0x5d, 0xc1, 0xb1, 0xd9, 0x9e, 0x5c, 0xaa, 0x0f,
	0xa1, 0x01, 0x5b, 0x8f, 0x5d, 0xfa, 0x8c, 0x06, 0x58, 0x7e, 0xeb, 0x89, 0xd6, 0xab, 0x27, 0x65,
	0xac, 0x2f, 0x17, 0x70, 0x72, 0xa9, 0xbe, 0x0e, 0xb7, 0x20, 0xff, 0x81, 0xd3, 0x77, 0x07, 0x58,
	0x7c, 0xb4, 0xf2, 0x73, 0xa1, 0xc9, 0xb8, 0x5a, 0x35, 0x8d, 0xea, 0x57, 0x8a, 0xdf, 0x6a, 0x6d,
	0xf1, 0x26, 0x78, 0xfb, 0xbf, 0x00, 0x00, 0x00, 0xff, 0xff, 0xee, 0xd4, 0x0a, 0x82, 0x69, 0x09,
	0x00, 0x00,
}

// Reference imports to suppress errors if they are not otherwise used.
var _ context.Context
var _ grpc.ClientConn

// This is a compile-time assertion to ensure that this generated file
// is compatible with the grpc package it is being compiled against.
const _ = grpc.SupportPackageIsVersion4

// ProjectsClient is the client API for Projects service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type ProjectsClient interface {
	// CreateProject creates new project.
	CreateProject(ctx context.Context, in *CreateProjectRequest, opts ...grpc.CallOption) (*Project, error)
	// GetProject returns the project by its unique id.
	GetProject(ctx context.Context, in *GetProjectRequest, opts ...grpc.CallOption) (*Project, error)
	// DeleteProject will delete the project from the system by Id.
	// This will be a soft delete from the system
	DeleteProject(ctx context.Context, in *DeleteProjectRequest, opts ...grpc.CallOption) (*empty.Empty, error)
	// UpdateProject will update the project identified by its project id.
	// Update Project uses Field Mask to update specific properties of project object
	UpdateProject(ctx context.Context, in *UpdateProjectRequest, opts ...grpc.CallOption) (*Project, error)
	// ListProject lists all the Project(s)
	ListProject(ctx context.Context, in *ListProjectRequest, opts ...grpc.CallOption) (*ListProjectResponse, error)
	// Gets all the Project(s) by their ids
	BatchGetProject(ctx context.Context, in *BatchGetProjectRequest, opts ...grpc.CallOption) (*BatchGetProjectResponse, error)
}

type projectsClient struct {
	cc *grpc.ClientConn
}

func NewProjectsClient(cc *grpc.ClientConn) ProjectsClient {
	return &projectsClient{cc}
}

func (c *projectsClient) CreateProject(ctx context.Context, in *CreateProjectRequest, opts ...grpc.CallOption) (*Project, error) {
	out := new(Project)
	err := c.cc.Invoke(ctx, "/saastack.project.v1.Projects/CreateProject", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *projectsClient) GetProject(ctx context.Context, in *GetProjectRequest, opts ...grpc.CallOption) (*Project, error) {
	out := new(Project)
	err := c.cc.Invoke(ctx, "/saastack.project.v1.Projects/GetProject", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *projectsClient) DeleteProject(ctx context.Context, in *DeleteProjectRequest, opts ...grpc.CallOption) (*empty.Empty, error) {
	out := new(empty.Empty)
	err := c.cc.Invoke(ctx, "/saastack.project.v1.Projects/DeleteProject", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *projectsClient) UpdateProject(ctx context.Context, in *UpdateProjectRequest, opts ...grpc.CallOption) (*Project, error) {
	out := new(Project)
	err := c.cc.Invoke(ctx, "/saastack.project.v1.Projects/UpdateProject", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *projectsClient) ListProject(ctx context.Context, in *ListProjectRequest, opts ...grpc.CallOption) (*ListProjectResponse, error) {
	out := new(ListProjectResponse)
	err := c.cc.Invoke(ctx, "/saastack.project.v1.Projects/ListProject", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (c *projectsClient) BatchGetProject(ctx context.Context, in *BatchGetProjectRequest, opts ...grpc.CallOption) (*BatchGetProjectResponse, error) {
	out := new(BatchGetProjectResponse)
	err := c.cc.Invoke(ctx, "/saastack.project.v1.Projects/BatchGetProject", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ProjectsServer is the server API for Projects service.
type ProjectsServer interface {
	// CreateProject creates new project.
	CreateProject(context.Context, *CreateProjectRequest) (*Project, error)
	// GetProject returns the project by its unique id.
	GetProject(context.Context, *GetProjectRequest) (*Project, error)
	// DeleteProject will delete the project from the system by Id.
	// This will be a soft delete from the system
	DeleteProject(context.Context, *DeleteProjectRequest) (*empty.Empty, error)
	// UpdateProject will update the project identified by its project id.
	// Update Project uses Field Mask to update specific properties of project object
	UpdateProject(context.Context, *UpdateProjectRequest) (*Project, error)
	// ListProject lists all the Project(s)
	ListProject(context.Context, *ListProjectRequest) (*ListProjectResponse, error)
	// Gets all the Project(s) by their ids
	BatchGetProject(context.Context, *BatchGetProjectRequest) (*BatchGetProjectResponse, error)
}

// UnimplementedProjectsServer can be embedded to have forward compatible implementations.
type UnimplementedProjectsServer struct {
}

func (*UnimplementedProjectsServer) CreateProject(ctx context.Context, req *CreateProjectRequest) (*Project, error) {
	return nil, status.Errorf(codes.Unimplemented, "method CreateProject not implemented")
}
func (*UnimplementedProjectsServer) GetProject(ctx context.Context, req *GetProjectRequest) (*Project, error) {
	return nil, status.Errorf(codes.Unimplemented, "method GetProject not implemented")
}
func (*UnimplementedProjectsServer) DeleteProject(ctx context.Context, req *DeleteProjectRequest) (*empty.Empty, error) {
	return nil, status.Errorf(codes.Unimplemented, "method DeleteProject not implemented")
}
func (*UnimplementedProjectsServer) UpdateProject(ctx context.Context, req *UpdateProjectRequest) (*Project, error) {
	return nil, status.Errorf(codes.Unimplemented, "method UpdateProject not implemented")
}
func (*UnimplementedProjectsServer) ListProject(ctx context.Context, req *ListProjectRequest) (*ListProjectResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ListProject not implemented")
}
func (*UnimplementedProjectsServer) BatchGetProject(ctx context.Context, req *BatchGetProjectRequest) (*BatchGetProjectResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method BatchGetProject not implemented")
}

func RegisterProjectsServer(s *grpc.Server, srv ProjectsServer) {
	s.RegisterService(&_Projects_serviceDesc, srv)
}

func _Projects_CreateProject_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(CreateProjectRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProjectsServer).CreateProject(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/saastack.project.v1.Projects/CreateProject",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProjectsServer).CreateProject(ctx, req.(*CreateProjectRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Projects_GetProject_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(GetProjectRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProjectsServer).GetProject(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/saastack.project.v1.Projects/GetProject",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProjectsServer).GetProject(ctx, req.(*GetProjectRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Projects_DeleteProject_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(DeleteProjectRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProjectsServer).DeleteProject(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/saastack.project.v1.Projects/DeleteProject",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProjectsServer).DeleteProject(ctx, req.(*DeleteProjectRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Projects_UpdateProject_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(UpdateProjectRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProjectsServer).UpdateProject(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/saastack.project.v1.Projects/UpdateProject",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProjectsServer).UpdateProject(ctx, req.(*UpdateProjectRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Projects_ListProject_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ListProjectRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProjectsServer).ListProject(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/saastack.project.v1.Projects/ListProject",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProjectsServer).ListProject(ctx, req.(*ListProjectRequest))
	}
	return interceptor(ctx, in, info, handler)
}

func _Projects_BatchGetProject_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(BatchGetProjectRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ProjectsServer).BatchGetProject(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/saastack.project.v1.Projects/BatchGetProject",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ProjectsServer).BatchGetProject(ctx, req.(*BatchGetProjectRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _Projects_serviceDesc = grpc.ServiceDesc{
	ServiceName: "saastack.project.v1.Projects",
	HandlerType: (*ProjectsServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "CreateProject",
			Handler:    _Projects_CreateProject_Handler,
		},
		{
			MethodName: "GetProject",
			Handler:    _Projects_GetProject_Handler,
		},
		{
			MethodName: "DeleteProject",
			Handler:    _Projects_DeleteProject_Handler,
		},
		{
			MethodName: "UpdateProject",
			Handler:    _Projects_UpdateProject_Handler,
		},
		{
			MethodName: "ListProject",
			Handler:    _Projects_ListProject_Handler,
		},
		{
			MethodName: "BatchGetProject",
			Handler:    _Projects_BatchGetProject_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "project.proto",
}

// ParentServiceClient is the client API for ParentService service.
//
// For semantics around ctx use and closing/ending streaming RPCs, please refer to https://godoc.org/google.golang.org/grpc#ClientConn.NewStream.
type ParentServiceClient interface {
	ValidateParent(ctx context.Context, in *ValidateParentRequest, opts ...grpc.CallOption) (*ValidateParentResponse, error)
}

type parentServiceClient struct {
	cc *grpc.ClientConn
}

func NewParentServiceClient(cc *grpc.ClientConn) ParentServiceClient {
	return &parentServiceClient{cc}
}

func (c *parentServiceClient) ValidateParent(ctx context.Context, in *ValidateParentRequest, opts ...grpc.CallOption) (*ValidateParentResponse, error) {
	out := new(ValidateParentResponse)
	err := c.cc.Invoke(ctx, "/saastack.project.v1.ParentService/ValidateParent", in, out, opts...)
	if err != nil {
		return nil, err
	}
	return out, nil
}

// ParentServiceServer is the server API for ParentService service.
type ParentServiceServer interface {
	ValidateParent(context.Context, *ValidateParentRequest) (*ValidateParentResponse, error)
}

// UnimplementedParentServiceServer can be embedded to have forward compatible implementations.
type UnimplementedParentServiceServer struct {
}

func (*UnimplementedParentServiceServer) ValidateParent(ctx context.Context, req *ValidateParentRequest) (*ValidateParentResponse, error) {
	return nil, status.Errorf(codes.Unimplemented, "method ValidateParent not implemented")
}

func RegisterParentServiceServer(s *grpc.Server, srv ParentServiceServer) {
	s.RegisterService(&_ParentService_serviceDesc, srv)
}

func _ParentService_ValidateParent_Handler(srv interface{}, ctx context.Context, dec func(interface{}) error, interceptor grpc.UnaryServerInterceptor) (interface{}, error) {
	in := new(ValidateParentRequest)
	if err := dec(in); err != nil {
		return nil, err
	}
	if interceptor == nil {
		return srv.(ParentServiceServer).ValidateParent(ctx, in)
	}
	info := &grpc.UnaryServerInfo{
		Server:     srv,
		FullMethod: "/saastack.project.v1.ParentService/ValidateParent",
	}
	handler := func(ctx context.Context, req interface{}) (interface{}, error) {
		return srv.(ParentServiceServer).ValidateParent(ctx, req.(*ValidateParentRequest))
	}
	return interceptor(ctx, in, info, handler)
}

var _ParentService_serviceDesc = grpc.ServiceDesc{
	ServiceName: "saastack.project.v1.ParentService",
	HandlerType: (*ParentServiceServer)(nil),
	Methods: []grpc.MethodDesc{
		{
			MethodName: "ValidateParent",
			Handler:    _ParentService_ValidateParent_Handler,
		},
	},
	Streams:  []grpc.StreamDesc{},
	Metadata: "project.proto",
}
