package pb

import (
	context "context"
	x "database/sql"

	sqrl "github.com/elgris/sqrl"
	timestamp "github.com/golang/protobuf/ptypes/timestamp"
	chaku_globals "go.saastack.io/chaku/chaku-globals"
	driver "go.saastack.io/chaku/driver"
	sql "go.saastack.io/chaku/driver/pgsql"
	errors "go.saastack.io/chaku/errors"
)

var objectTableMap = chaku_globals.ObjectTable{
	"project": {
		"id":    "project",
		"title": "project",
		"color": "project",
	},
}

func (m *Project) PackageName() string {
	return "saastack_project_v1"
}

func (m *Project) TableOfObject(f, s string) string {
	return objectTableMap[f][s]
}

func (m *Project) GetDescriptorsOf(f string) (driver.Descriptor, error) {
	switch f {
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Project) ObjectName() string {
	return "project"
}

func (m *Project) Fields() []string {
	return []string{
		"id", "title", "color",
	}
}

func (m *Project) IsObject(field string) bool {
	switch field {
	default:
		return false
	}
}

func (m *Project) ValuerSlice(field string) ([]driver.Descriptor, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	default:
		return []driver.Descriptor{}, errors.ErrInvalidField
	}
}

func (m *Project) Valuer(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return m.Id, nil
	case "title":
		return m.Title, nil
	case "color":
		return m.Color, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Project) Addresser(field string) (interface{}, error) {
	if m == nil {
		return nil, nil
	}
	switch field {
	case "id":
		return &m.Id, nil
	case "title":
		return &m.Title, nil
	case "color":
		return &m.Color, nil
	default:
		return nil, errors.ErrInvalidField
	}
}

func (m *Project) New(field string) error {
	switch field {
	case "id":
		return nil
	case "title":
		return nil
	case "color":
		return nil
	default:
		return errors.ErrInvalidField
	}
}

func (m *Project) Type(field string) string {
	switch field {
	case "id":
		return "string"
	case "title":
		return "string"
	case "color":
		return "string"
	default:
		return ""
	}
}

func (_ *Project) GetEmptyObject() (m *Project) {
	m = &Project{}
	return
}

func (m *Project) GetPrefix() string {
	return "pro"
}

func (m *Project) GetID() string {
	return m.Id
}

func (m *Project) SetID(id string) {
	m.Id = id
}

func (m *Project) IsRoot() bool {
	return true
}

func (m *Project) IsFlatObject(f string) bool {
	return false
}

func (m *Project) NoOfParents(d driver.Descriptor) int {
	switch d.ObjectName() {
	}
	return 0
}

type ProjectStore struct {
	d      driver.Driver
	withTx bool
	tx     driver.Transaction

	limitMultiplier int
}

func (s ProjectStore) Execute(ctx context.Context, query string, args ...interface{}) error {
	if s.withTx {
		return s.tx.Execute(ctx, query, args...)
	}
	return s.d.Execute(ctx, query, args...)
}

func (s ProjectStore) QueryRows(ctx context.Context, query string, scanners []string, args ...interface{}) (driver.Result, error) {
	if s.withTx {
		return s.tx.QueryRows(ctx, query, scanners, args...)
	}
	return s.d.QueryRows(ctx, query, scanners, args...)
}

func NewProjectStore(d driver.Driver) ProjectStore {
	return ProjectStore{d: d, limitMultiplier: 1}
}

func NewPostgresProjectStore(db *x.DB, usr driver.IUserInfo) ProjectStore {
	return ProjectStore{
		d:               &sql.Sql{DB: db, UserInfo: usr, Placeholder: sqrl.Dollar},
		limitMultiplier: 1,
	}
}

type ProjectTx struct {
	ProjectStore
}

func (s ProjectStore) BeginTx(ctx context.Context) (*ProjectTx, error) {
	tx, err := s.d.BeginTx(ctx)
	if err != nil {
		return nil, err
	}
	return &ProjectTx{
		ProjectStore: ProjectStore{
			d:      s.d,
			withTx: true,
			tx:     tx,
		},
	}, nil
}

func (tx *ProjectTx) Commit(ctx context.Context) error {
	return tx.tx.Commit(ctx)
}

func (tx *ProjectTx) RollBack(ctx context.Context) error {
	return tx.tx.RollBack(ctx)
}

func (s ProjectStore) CreateProjectPGStore(ctx context.Context) error {
	const queries = `
CREATE SCHEMA IF NOT EXISTS saastack_project_v1;
CREATE TABLE IF NOT EXISTS  saastack_project_v1.project( id text DEFAULT ''::text , title text DEFAULT ''::text , color text DEFAULT ''::text , parent text DEFAULT ''::text , is_deleted boolean DEFAULT false, deleted_by text DEFAULT ''::text, deleted_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, updated_by text DEFAULT ''::text, updated_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, created_by text DEFAULT ''::text, created_on timestamp without time zone DEFAULT '0001-01-01 00:00:00'::timestamp without time zone, field_variable_mask text DEFAULT ''::text, PRIMARY KEY (id, parent)); 
CREATE TABLE IF NOT EXISTS  saastack_project_v1.project_parent( id text DEFAULT ''::text , parent text DEFAULT ''::text ); 
`
	if err := s.d.Execute(ctx, queries); err != nil {
		return err
	}
	return nil
}

func (s ProjectStore) CreateProjects(ctx context.Context, list ...*Project) ([]string, error) {
	vv := make([]driver.Descriptor, len(list))
	for i := range list {
		vv[i] = list[i]
	}
	if s.withTx {
		return s.tx.Insert(ctx, vv, &Project{}, &Project{}, "", []string{})
	}
	return s.d.Insert(ctx, vv, &Project{}, &Project{}, "", []string{})
}

func (s ProjectStore) DeleteProject(ctx context.Context, cond ProjectCondition) error {
	if s.withTx {
		return s.tx.Delete(ctx, cond.projectCondToDriverProjectCond(s.d), &Project{}, &Project{})
	}
	return s.d.Delete(ctx, cond.projectCondToDriverProjectCond(s.d), &Project{}, &Project{})
}

func (s ProjectStore) UpdateProject(ctx context.Context, req *Project, fields []string, cond ProjectCondition) error {
	if s.withTx {
		return s.tx.Update(ctx, cond.projectCondToDriverProjectCond(s.d), req, &Project{}, fields...)
	}
	return s.d.Update(ctx, cond.projectCondToDriverProjectCond(s.d), req, &Project{}, fields...)
}

func (s ProjectStore) UpdateProjectMetaInfo(ctx context.Context, list ...*driver.UpdateMetaInfoRequest) error {
	fn := s.d.UpdateMetaInfo
	if s.withTx {
		fn = s.tx.UpdateMetaInfo
	}
	return fn(ctx, &Project{}, &Project{}, list...)
}

func (s ProjectStore) GetProject(ctx context.Context, fields []string, cond ProjectCondition, opt ...getProjectsOption) (*Project, error) {
	if len(fields) == 0 {
		fields = (&Project{}).Fields()
	}
	m := MetaInfoForList{}
	listOpts := []listProjectsOption{
		&CursorBasedPagination{Limit: 1},
	}
	for _, o := range opt {
		t, _ := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			listOpts = append(listOpts, &m)
		}
	}
	objList, err := s.ListProjects(ctx, fields, cond, listOpts...)
	if len(objList) == 0 && err == nil {
		err = errors.ErrNotFound
	}
	if err != nil {
		return nil, err
	}
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_MetaInfo:
			in.(*MetaInfo).UpdatedBy = m[0].UpdatedBy
			in.(*MetaInfo).CreatedBy = m[0].CreatedBy
			in.(*MetaInfo).DeletedBy = m[0].DeletedBy
			in.(*MetaInfo).UpdatedOn = m[0].UpdatedOn
			in.(*MetaInfo).CreatedOn = m[0].CreatedOn
			in.(*MetaInfo).DeletedOn = m[0].DeletedOn
			in.(*MetaInfo).IsDeleted = m[0].IsDeleted
		}
	}
	return objList[0], nil
}

func (s ProjectStore) ListProjects(ctx context.Context, fields []string, cond ProjectCondition, opt ...listProjectsOption) ([]*Project, error) {
	if len(fields) == 0 {
		fields = (&Project{}).Fields()
	}
	var (
		res driver.Result
		err error
		m   driver.MetaInfo

		limit       = -1
		orderByList = make([]driver.OrderByType, 0, 5)
	)
	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if page.SetProjectCondition == nil {
					page.SetProjectCondition = defaultSetProjectCondition
				}
				cond = page.SetProjectCondition(page.UpOrDown, page.Cursor, cond)
				limit = page.Limit + 1
				if len(orderByList) == 0 {
					orderByList = append(orderByList, driver.OrderByType{
						Field:     "id",
						Ascending: !page.UpOrDown,
					})
				}
			}
		case driver.OptionType_MetaInfo:
			s.d.MetaInfoRequested(&ctx, &m)
		case driver.OptionType_OrderBy:
			by, ok := in.(OrderBy)
			if ok && len(by.Bys) != 0 {
				orderByList = by.Bys
			}
		}
	}
	if len(orderByList) == 0 {
		orderByList = append(orderByList, driver.OrderByType{
			Field:     "id",
			Ascending: true,
		})
	}
	ctx = driver.SetOrderBy(ctx, orderByList...)
	if limit > 0 {
		ctx = driver.SetListLimit(ctx, limit)
	}

	if s.withTx {
		res, err = s.tx.Get(ctx, cond.projectCondToDriverProjectCond(s.d), &Project{}, &Project{}, fields...)
	} else {
		res, err = s.d.Get(ctx, cond.projectCondToDriverProjectCond(s.d), &Project{}, &Project{}, fields...)
	}
	if err != nil {
		return nil, err
	}
	defer res.Close()

	mp := map[string]struct{}{}
	list := make([]*Project, 0, 1000)
	infoMap := make(map[string]*driver.MetaInfo, 0)

	for res.Next(ctx) && limit != 0 {
		obj := &Project{}
		if err := res.Scan(ctx, obj); err != nil {
			return nil, err
		}
		for _, o := range opt {
			t, _ := o.getValue()
			switch t {
			case driver.OptionType_MetaInfo:
				infoMap[obj.Id] = &driver.MetaInfo{
					UpdatedBy: m.UpdatedBy,
					CreatedBy: m.CreatedBy,
					DeletedBy: m.DeletedBy,
					UpdatedOn: m.UpdatedOn,
					CreatedOn: m.CreatedOn,
					DeletedOn: m.DeletedOn,
					IsDeleted: m.IsDeleted,
				}
				break
			}
		}
		list = append(list, obj)
		if _, ok := mp[obj.Id]; !ok {
			limit--
			mp[obj.Id] = struct{}{}
		}
	}
	if err := res.Close(); err != nil {
		return nil, err
	}

	list = MapperProject(list)
	meta := &MetaInfoForList{}

	for _, o := range opt {
		t, in := o.getValue()
		switch t {
		case driver.OptionType_Pagination:
			page, ok := in.(*CursorBasedPagination)
			if page != nil && ok {
				if len(list) <= page.Limit {
					page.HasNext = false
					page.HasPrevious = false
				} else {
					list = list[:page.Limit]
					if page.UpOrDown {
						page.HasPrevious = true
					} else {
						page.HasNext = true
					}
				}
			}
		case driver.OptionType_MetaInfo:
			meta = in.(*MetaInfoForList)
		}
	}
	for _, l := range list {
		*meta = append(*meta, infoMap[l.Id])
	}
	return list, nil
}

func (s ProjectStore) CountProjects(ctx context.Context, cond ProjectCondition) (int, error) {
	cntFn := s.d.Count
	if s.withTx {
		cntFn = s.tx.Count
	}
	return cntFn(ctx, cond.projectCondToDriverProjectCond(s.d), &Project{}, &Project{})
}

type getProjectsOption interface {
	getOptProjects() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfo) getOptProjects() { // method of no significant use
}

type listProjectsOption interface {
	listOptProjects() // method of no significant use
	getValue() (driver.OptionType, interface{})
}

func (*MetaInfoForList) listOptProjects() {
}

func (OrderBy) listOptProjects() {
}

func (*CursorBasedPagination) listOptProjects() {
}

func defaultSetProjectCondition(upOrDown bool, cursor string, cond ProjectCondition) ProjectCondition {
	if upOrDown {
		if cursor != "" {
			return ProjectAnd{cond, ProjectIdLt{cursor}}
		}
		return cond
	}
	if cursor != "" {
		return ProjectAnd{cond, ProjectIdGt{cursor}}
	}
	return cond
}

type ProjectAnd []ProjectCondition

func (p ProjectAnd) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.projectCondToDriverProjectCond(d))
	}
	return driver.And{Conditioners: dc, Operator: d}
}

type ProjectOr []ProjectCondition

func (p ProjectOr) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	dc := make([]driver.Conditioner, 0, len(p))
	for _, c := range p {
		dc = append(dc, c.projectCondToDriverProjectCond(d))
	}
	return driver.Or{Conditioners: dc, Operator: d}
}

type ProjectParentEq struct {
	Parent string
}

func (c ProjectParentEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectFullParentEq struct {
	Parent string
}

func (c ProjectFullParentEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectParentNotEq struct {
	Parent string
}

func (c ProjectParentNotEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectFullParentNotEq struct {
	Parent string
}

func (c ProjectFullParentNotEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectParentLike struct {
	Parent string
}

func (c ProjectParentLike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectFullParentLike struct {
	Parent string
}

func (c ProjectFullParentLike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectParentILike struct {
	Parent string
}

func (c ProjectParentILike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectFullParentILike struct {
	Parent string
}

func (c ProjectFullParentILike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectParentIn struct {
	Parent []string
}

func (c ProjectParentIn) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectFullParentIn struct {
	Parent []string
}

func (c ProjectFullParentIn) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectParentNotIn struct {
	Parent []string
}

func (c ProjectParentNotIn) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "parent", Value: c.Parent, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectFullParentNotIn struct {
	Parent []string
}

func (c ProjectFullParentNotIn) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "fullParent", Value: c.Parent, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectIdEq struct {
	Id string
}

func (c ProjectIdEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Project{}, FieldMask: "id", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectTitleEq struct {
	Title string
}

func (c ProjectTitleEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Project{}, FieldMask: "title", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectColorEq struct {
	Color string
}

func (c ProjectColorEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "color", Value: c.Color, Operator: d, Descriptor: &Project{}, FieldMask: "color", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectIdNotEq struct {
	Id string
}

func (c ProjectIdNotEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Project{}, FieldMask: "id", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectTitleNotEq struct {
	Title string
}

func (c ProjectTitleNotEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Project{}, FieldMask: "title", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectColorNotEq struct {
	Color string
}

func (c ProjectColorNotEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "color", Value: c.Color, Operator: d, Descriptor: &Project{}, FieldMask: "color", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectIdGt struct {
	Id string
}

func (c ProjectIdGt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Project{}, FieldMask: "id", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectTitleGt struct {
	Title string
}

func (c ProjectTitleGt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "title", Value: c.Title, Operator: d, Descriptor: &Project{}, FieldMask: "title", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectColorGt struct {
	Color string
}

func (c ProjectColorGt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "color", Value: c.Color, Operator: d, Descriptor: &Project{}, FieldMask: "color", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectIdLt struct {
	Id string
}

func (c ProjectIdLt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "id", Value: c.Id, Operator: d, Descriptor: &Project{}, FieldMask: "id", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectTitleLt struct {
	Title string
}

func (c ProjectTitleLt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "title", Value: c.Title, Operator: d, Descriptor: &Project{}, FieldMask: "title", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectColorLt struct {
	Color string
}

func (c ProjectColorLt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "color", Value: c.Color, Operator: d, Descriptor: &Project{}, FieldMask: "color", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectIdGtOrEq struct {
	Id string
}

func (c ProjectIdGtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Project{}, FieldMask: "id", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectTitleGtOrEq struct {
	Title string
}

func (c ProjectTitleGtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Project{}, FieldMask: "title", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectColorGtOrEq struct {
	Color string
}

func (c ProjectColorGtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "color", Value: c.Color, Operator: d, Descriptor: &Project{}, FieldMask: "color", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectIdLtOrEq struct {
	Id string
}

func (c ProjectIdLtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "id", Value: c.Id, Operator: d, Descriptor: &Project{}, FieldMask: "id", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectTitleLtOrEq struct {
	Title string
}

func (c ProjectTitleLtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "title", Value: c.Title, Operator: d, Descriptor: &Project{}, FieldMask: "title", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectColorLtOrEq struct {
	Color string
}

func (c ProjectColorLtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "color", Value: c.Color, Operator: d, Descriptor: &Project{}, FieldMask: "color", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectIdLike struct {
	Id string
}

func (c ProjectIdLike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "id", Value: c.Id, Operator: d, Descriptor: &Project{}, FieldMask: "id", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectTitleLike struct {
	Title string
}

func (c ProjectTitleLike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "title", Value: c.Title, Operator: d, Descriptor: &Project{}, FieldMask: "title", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectColorLike struct {
	Color string
}

func (c ProjectColorLike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "color", Value: c.Color, Operator: d, Descriptor: &Project{}, FieldMask: "color", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectIdILike struct {
	Id string
}

func (c ProjectIdILike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "id", Value: c.Id, Operator: d, Descriptor: &Project{}, FieldMask: "id", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectTitleILike struct {
	Title string
}

func (c ProjectTitleILike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "title", Value: c.Title, Operator: d, Descriptor: &Project{}, FieldMask: "title", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectColorILike struct {
	Color string
}

func (c ProjectColorILike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "color", Value: c.Color, Operator: d, Descriptor: &Project{}, FieldMask: "color", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeleted struct {
	IsDeleted bool
}

func (c ProjectDeleted) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "is_deleted", Value: c.IsDeleted, Operator: d, Descriptor: &Project{}, FieldMask: "id", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectCreatedByEq struct {
	By string
}

func (c ProjectCreatedByEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectCreatedOnEq struct {
	On *timestamp.Timestamp
}

func (c ProjectCreatedOnEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectCreatedByNotEq struct {
	By string
}

func (c ProjectCreatedByNotEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectCreatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c ProjectCreatedOnNotEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectCreatedByGt struct {
	By string
}

func (c ProjectCreatedByGt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectCreatedOnGt struct {
	On *timestamp.Timestamp
}

func (c ProjectCreatedOnGt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectCreatedByLt struct {
	By string
}

func (c ProjectCreatedByLt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectCreatedOnLt struct {
	On *timestamp.Timestamp
}

func (c ProjectCreatedOnLt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectCreatedByGtOrEq struct {
	By string
}

func (c ProjectCreatedByGtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectCreatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c ProjectCreatedOnGtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectCreatedByLtOrEq struct {
	By string
}

func (c ProjectCreatedByLtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectCreatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c ProjectCreatedOnLtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "created_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectCreatedByLike struct {
	By string
}

func (c ProjectCreatedByLike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectCreatedByILike struct {
	By string
}

func (c ProjectCreatedByILike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "created_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectUpdatedByEq struct {
	By string
}

func (c ProjectUpdatedByEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectUpdatedOnEq struct {
	On *timestamp.Timestamp
}

func (c ProjectUpdatedOnEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectUpdatedByNotEq struct {
	By string
}

func (c ProjectUpdatedByNotEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectUpdatedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c ProjectUpdatedOnNotEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectUpdatedByGt struct {
	By string
}

func (c ProjectUpdatedByGt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectUpdatedOnGt struct {
	On *timestamp.Timestamp
}

func (c ProjectUpdatedOnGt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectUpdatedByLt struct {
	By string
}

func (c ProjectUpdatedByLt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectUpdatedOnLt struct {
	On *timestamp.Timestamp
}

func (c ProjectUpdatedOnLt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectUpdatedByGtOrEq struct {
	By string
}

func (c ProjectUpdatedByGtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectUpdatedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c ProjectUpdatedOnGtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectUpdatedByLtOrEq struct {
	By string
}

func (c ProjectUpdatedByLtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectUpdatedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c ProjectUpdatedOnLtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "updated_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectUpdatedByLike struct {
	By string
}

func (c ProjectUpdatedByLike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectUpdatedByILike struct {
	By string
}

func (c ProjectUpdatedByILike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "updated_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeletedByEq struct {
	By string
}

func (c ProjectDeletedByEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeletedOnEq struct {
	On *timestamp.Timestamp
}

func (c ProjectDeletedOnEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Eq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeletedByNotEq struct {
	By string
}

func (c ProjectDeletedByNotEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeletedOnNotEq struct {
	On *timestamp.Timestamp
}

func (c ProjectDeletedOnNotEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeletedByGt struct {
	By string
}

func (c ProjectDeletedByGt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeletedOnGt struct {
	On *timestamp.Timestamp
}

func (c ProjectDeletedOnGt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Gt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeletedByLt struct {
	By string
}

func (c ProjectDeletedByLt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeletedOnLt struct {
	On *timestamp.Timestamp
}

func (c ProjectDeletedOnLt) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Lt{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeletedByGtOrEq struct {
	By string
}

func (c ProjectDeletedByGtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeletedOnGtOrEq struct {
	On *timestamp.Timestamp
}

func (c ProjectDeletedOnGtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.GtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeletedByLtOrEq struct {
	By string
}

func (c ProjectDeletedByLtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeletedOnLtOrEq struct {
	On *timestamp.Timestamp
}

func (c ProjectDeletedOnLtOrEq) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.LtOrEq{Key: "deleted_on", Value: c.On, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeletedByLike struct {
	By string
}

func (c ProjectDeletedByLike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.Like{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectDeletedByILike struct {
	By string
}

func (c ProjectDeletedByILike) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.ILike{Key: "deleted_by", Value: c.By, Operator: d, Descriptor: &Project{}, RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectIdIn struct {
	Id []string
}

func (c ProjectIdIn) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "id", Value: c.Id, Operator: d, Descriptor: &Project{}, FieldMask: "id", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectTitleIn struct {
	Title []string
}

func (c ProjectTitleIn) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "title", Value: c.Title, Operator: d, Descriptor: &Project{}, FieldMask: "title", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectColorIn struct {
	Color []string
}

func (c ProjectColorIn) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.In{Key: "color", Value: c.Color, Operator: d, Descriptor: &Project{}, FieldMask: "color", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectIdNotIn struct {
	Id []string
}

func (c ProjectIdNotIn) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "id", Value: c.Id, Operator: d, Descriptor: &Project{}, FieldMask: "id", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectTitleNotIn struct {
	Title []string
}

func (c ProjectTitleNotIn) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "title", Value: c.Title, Operator: d, Descriptor: &Project{}, FieldMask: "title", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

type ProjectColorNotIn struct {
	Color []string
}

func (c ProjectColorNotIn) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.NotIn{Key: "color", Value: c.Color, Operator: d, Descriptor: &Project{}, FieldMask: "color", RootDescriptor: &Project{}, CurrentDescriptor: &Project{}}
}

func (c TrueCondition) projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner {
	return driver.TrueCondition{Operator: d}
}

type projectMapperObject struct {
	id    string
	title string
	color string
}

func (s *projectMapperObject) GetUniqueIdentifier() string {
	return s.id
}

func MapperProject(rows []*Project) []*Project {

	ids := make([]string, 0, len(rows))
	uniqueIDMap := map[string]bool{}
	for _, r := range rows {
		if uniqueIDMap[r.Id] {
			continue
		}
		uniqueIDMap[r.Id] = true
		ids = append(ids, r.Id)
	}

	combinedProjectMappers := map[string]*projectMapperObject{}

	for _, rw := range rows {

		tempProject := &projectMapperObject{}

		if rw == nil {
			rw = rw.GetEmptyObject()
		}
		tempProject.id = rw.Id
		tempProject.title = rw.Title
		tempProject.color = rw.Color

		if combinedProjectMappers[tempProject.GetUniqueIdentifier()] == nil {
			combinedProjectMappers[tempProject.GetUniqueIdentifier()] = tempProject
		}
	}

	combinedProjects := make(map[string]*Project, 0)

	for _, project := range combinedProjectMappers {
		tempProject := &Project{}
		tempProject.Id = project.id
		tempProject.Title = project.title
		tempProject.Color = project.color

		if tempProject.Id == "" {
			continue
		}

		combinedProjects[tempProject.Id] = tempProject

	}
	list := make([]*Project, 0, len(combinedProjects))
	for _, i := range ids {
		list = append(list, combinedProjects[i])
	}
	return list
}

func (m *Project) IsUsedMultipleTimes(f string) bool {
	return false
}

type TrueCondition struct{}

type ProjectCondition interface {
	projectCondToDriverProjectCond(d driver.Driver) driver.Conditioner
}

type CursorBasedPagination struct {
	// Set UpOrDown = true for getting list of data above Cursor-ID,
	// limited to 'limit' amount, when ordered by ID in Ascending order.
	// Set UpOrDown = false for getting list of data below Cursor-ID,
	// limited to 'limit' amount, when ordered by ID in Ascending order.
	Cursor   string
	Limit    int
	UpOrDown bool

	// All pagination-cursor condition functions for different objects
	// SetProjectCondition will be used to set the condition parameter for
	// setting parameter based on UpOrDown value,
	// if null default IdGt or IdLt condition will be used.
	SetProjectCondition func(upOrDown bool, cursor string, cond ProjectCondition) ProjectCondition

	// Response objects Items - will be updated and set after the list call
	HasNext     bool // Used in case of UpOrDown = false
	HasPrevious bool // Used in case of UpOrDown = true
}

func (p *CursorBasedPagination) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_Pagination, p
}

type MetaInfo driver.MetaInfo
type MetaInfoForList []*driver.MetaInfo

func (p *MetaInfo) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_MetaInfo, p
}

func (p *MetaInfoForList) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_MetaInfo, p
}

type OrderBy struct {
	Bys []driver.OrderByType
}

func (o OrderBy) getValue() (driver.OptionType, interface{}) {
	return driver.OptionType_OrderBy, o
}
